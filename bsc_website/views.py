from django.http import HttpResponse


def health(request):
    return HttpResponse("ok", content_type="text/plain")


def well_known_server(request):
    data = """{
    "m.server": "burkesoftware.ems.host:443"
}"""
    return HttpResponse(data, content_type="text/plain")


def well_known_client(request):
    data = """{
    "m.homeserver": {
        "base_url": "https://burkesoftware.ems.host"
    },
    "m.identity_server": {
        "base_url": "https://vector.im"
    }
}"""
    res = HttpResponse(data, content_type="text/plain")
    res.headers['Access-Control-Allow-Origin'] = "*"
    return res
