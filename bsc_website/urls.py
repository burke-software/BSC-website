from django.conf.urls.static import static
from django.urls import path, re_path, include
from django.views.generic.base import RedirectView
from django.contrib import admin
from django.conf import settings
import os.path

from wagtail import urls as wagtail_urls
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.documents import urls as wagtaildocs_urls

from .views import health, well_known_client, well_known_server

admin.autodiscover()


urlpatterns = [
    path("_health/", health),
    path('django-admin/', admin.site.urls),
    path(".well-known/matrix/server", well_known_server),
    path(".well-known/matrix/client", well_known_client),
    re_path(r'^admin/', include(wagtailadmin_urls)),
    re_path(r'^documents/', include(wagtaildocs_urls)),
    path('accounts/', include('allauth.urls')),
    re_path(r'', include(wagtail_urls)),
]


if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns() # tell gunicorn where static files are in dev mode
    urlpatterns += static(settings.MEDIA_URL + 'images/', document_root=os.path.join(settings.MEDIA_ROOT, 'images'))
    urlpatterns += [
        re_path(r'^favicon\.ico$', RedirectView.as_view(url=settings.STATIC_URL + 'website/images/favicon.ico'))
    ]
