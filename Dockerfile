FROM python:3.12-slim
ENV PYTHONUNBUFFERED=1 \
  POETRY_VIRTUALENVS_CREATE=false \
  PIP_DISABLE_PIP_VERSION_CHECK=on
ARG IGNORE_DEV_DEPS

RUN mkdir /code
WORKDIR /code

RUN apt-get update && apt-get install -y gcc zlib1g-dev libjpeg-dev
RUN pip install poetry
COPY poetry.lock pyproject.toml /code/
RUN poetry install --no-interaction --no-ansi $(test "$IGNORE_DEV_DEPS" = "True" && echo "--no-dev")

EXPOSE 8080

ADD . /code/

CMD ["./bin/start.sh"]
